# AquaRest

> simple rest api for aquabox

## Install

web app
```bash
composer install
cp .env.example .env
./set_perms.sh
```

config app
```bash
cp settings-sample.json settings.json
vim settings.json
```

allow apache to use gpio
```bash
adduser www-data gpio
sudo /etc/init.d/apache2 restart
```

Add vhost in apache

## Use

Go to url and use link

## Todo

## Refs
