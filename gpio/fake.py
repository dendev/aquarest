#!/usr/bin/env python3
import sys
import json

def run(pin, action_slug, default='off'):
    inverted = True if default == 'on' else False

    state = None
    if action == 'on':
        state =  1 if inverted else 0
    elif action == 'off':
        state = 0 if inverted else 1
    else:
        state = 1

    label = 'on' if ( inverted and state == 1 ) or ( not inverted and state == 0 ) else 'off'

    infos = {
        'success': True,
        'msg' : 'Action done!',
        'pin' : pin,
        'action_slug' : action_slug,
        'default' : default,
        'state' : state,
        'label' : label
    }
    print(json.dumps(infos))

    return True

if( len(sys.argv) == 4 ):
    pin = str(sys.argv[1])
    action = str(sys.argv[2])
    default = str(sys.argv[3])

    run(pin, action, default)
else:
    infos = { 'success': False, 'msg' : 'missing arg! need pin, action, default', 'debug' : sys.argv }
    print(json.dumps(infos))

