#!/usr/bin/env python3

import sys, getopt
import RPi.GPIO as GPIO
import json


def run(pin, action_slug, default):
    GPIO.setwarnings(False)

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.OUT)

    inverted = True if default == 'on' else False

    # fix brume pin ( badly mounted ?! )
    inverted = True if pin == 22 else inverted

    if action_slug != 'state' :
        if action_slug == 'on':
            GPIO.output(pin, GPIO.HIGH if inverted else GPIO.LOW)
        elif action_slug == 'off':
            GPIO.output(pin, GPIO.LOW if inverted else GPIO.HIGH)
        elif action_slug == 'toggle':
            state = GPIO.input(pin)
            if( state == 0 ):
                GPIO.output(pin, GPIO.HIGH)
            else:
                GPIO.output(pin, GPIO.LOW)
        else:
            help()

    success = True

    state = GPIO.input(pin)
    label = 'on' if ( inverted and state == 1 ) or ( not inverted and state == 0 ) else 'off'

    infos = {
            'success': success,
            'msg' : 'Action done!',
            'pin' : pin,
            'action_slug' : action_slug,
            'default' : default,
            'state' : state,
            'label' : label
    }
    print(json.dumps(infos))

    return success
    #GPIO.cleanup()

def help():
    print( '*Relay usage: ' )
    print( 'run( pin, action_slug, default )' )
    print( 'available actions : on, off, toggle, state' )

if( len(sys.argv) == 4 ):
    pin = int(sys.argv[1])
    action_slug = str(sys.argv[2])
    default = str(sys.argv[3])

    run(pin, action_slug, default)
else:
    infos = { 'success': False, 'msg' : 'missing arg! need pin, action_slug, default', 'debug' : sys.argv }
    print(json.dumps(infos))

