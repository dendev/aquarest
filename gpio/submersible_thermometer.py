import sys, getopt
import os
import glob
import time
import json

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

def run(pin, action_slug, identity):
    temp_c = read_temp(identity)

    infos = {
            'success': True,
            'msg' : 'Action done!',
            'pin' : pin,
            'action_slug' : action_slug,
            'state' : temp_c,
            'label' : temp_c
    }
    print(json.dumps(infos))

    return True

def read_temp_raw(identity):
    base_dir = '/sys/bus/w1/devices/'
    device_folder = glob.glob(base_dir + identity)[0]
    device_file = device_folder + '/w1_slave'

    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp(identity):
    lines = read_temp_raw(identity)

    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw(identity)

    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        #return temp_c, temp_f

    return temp_c

def help():
    print( '*Submersible  usage: ' )
    print( 'run( pin, action_slug, identity )' )
    print( 'available actions : on, off, toggle, state' )

if( len(sys.argv) == 4 ):
    pin = int(sys.argv[1])
    action_slug = str(sys.argv[2])
    identity = str(sys.argv[3])

    run(pin, action_slug, identity)
else:
    infos = { 'success': False, 'msg' : 'missing arg! need pin, action_slug, identity', 'debug' : sys.argv }
    print(json.dumps(infos))


# refs
# DSB18B20
# https://medium.com/initial-state/how-to-build-a-raspberry-pi-temperature-monitor-8c2f70acaea9
# https://www.jeremymorgan.com/tutorials/raspberry-pi/monitor-room-temperature-raspberry-pi/
