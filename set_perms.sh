#!/bin/bash 

ROOT_PATH='./'

sudo chmod 775 -R "${ROOT_PATH}storage/${*}"
sudo chown $USER:www-data -R "${ROOT_PATH}storage/${*}"

sudo chmod 775 -R "${ROOT_PATH}gpio/${*}"
sudo chown $USER:www-data -R "${ROOT_PATH}gpio/${*}"

exit $?
