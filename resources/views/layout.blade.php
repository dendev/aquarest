<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ 'vendors/bootstrap/js/bootstrap.js' }}"></script>
    <script src="{{ 'js/app.js' }}" defer></script>
    @stack('scripts')

    <!-- Fonts -->

    <!-- Styles -->
    <link href="{{ 'css/app.css' }}" rel="stylesheet">
</head>
<body>

    <nav class="navbar navbar-light bg-primary mb-5" style="height: 80px">
        <div class="container-fluid">
            <span class="navbar-brand mb-0 h1 text-capitalize text-white">{{config('app.name', 'Laravel')}}</span>
        </div>
    </nav>

    <div class="container">
        @yield('content')
    </div>

    <!-- js -->
    @yield('scripts')
</body>
</html>
