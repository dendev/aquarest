@extends('layout')

@section('content')
    <div class="row">
        <div class="col-12">
            <h1>{{$title}}</h1>
            <h2>{{$description}}</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            @foreach( $resources as $resource )
                <div class="card mt-4">
                    <div class="card-body">
                        <h2 class="card-title">{{$resource['label']}}</h2>
                        <h3 class="card-subtitle mb-3">{{$resource['description']}}</h3>

                        @foreach( $resource['actions'] as $action )
                            @php $action_url = "/api/" . $resource['slug'] . "/" . $action['slug']; @endphp
                            <p><a href="{{$action_url}}" target="_blank">{{$action_url}}</a></p>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
