<?php


namespace App\Http\Controllers;


class RegisterController extends Controller
{
    public function show()
    {
        $settings_file_path = __DIR__ . '/../../../settings.json';

        $settings = [];
        if(  file_exists($settings_file_path) )
        {
            $settings = json_decode(file_get_contents($settings_file_path), true);
        }
        else
        {
            \Log::error('[RegisterController:show] : No setting.json file found!', [
                'settings_file_path' => $settings_file_path
            ]);
        }

        return response()->json($settings);
    }
}
