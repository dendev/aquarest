<?php


namespace App\Http\Controllers;


class GenericController extends Controller
{
    public function run($resource_slug, $action_slug)
    {
        $result = \ResourceManager::run($resource_slug, $action_slug);

        $code = 503;
        if( is_array($result) && $result['success'] )
            $code = 200;

        return response()->json($result, $code);
    }
}
