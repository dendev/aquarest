<?php


namespace App\Http\Controllers;


class HomeController extends Controller
{
    public function show()
    {
        $settings_file_path = __DIR__ . '/../../../settings.json';

        $resources = [];
        if(  file_exists($settings_file_path) )
        {
            $settings = json_decode(file_get_contents($settings_file_path), true);

            $title = $settings['label'];
            $description = $settings['description'];

            $resources = $settings['resources'];
        }
        else
        {
            \Log::error("[HomeController:show] no settings.json found",[
                'settings_file_path' => $settings_file_path
            ]);
        }

        return view('home', ['title' => $title, 'description' => $description, 'resources' => $resources]);
    }
}
