<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ResourceManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'resource_manager';
    }
}
