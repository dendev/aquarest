<?php

namespace App\Services;

class ResourceManagerService
{
    public function run($resource_slug, $action_slug)
    {
        $result = false;

        $resource = $this->get_resource_by_slug($resource_slug);
        $action = $this->get_action_by_slug($action_slug, $resource);

        if( $resource && $action )
        {
            $cmd = $this->_make_cmd($resource, $action);
            $output = $this->_run_cmd($cmd);
            $result = $this->_format_result($output, $resource, $action);
        }
        else
        {
            \Log::error("[ResourceManagerService:run] Bad resource or action", [
                'resource' => $resource,
                'action' => $action
            ]);
        }


        return $result;
    }

    public function get_resource_by_slug($slug)
    {
        $found = false;

        $resources = $this->_get_ressources();
        foreach( $resources as $resource )
        {
            if( $resource['slug'] === $slug )
            {
                $found = $resource;
                break;
            }
        }

        return $found;
    }

    public function get_action_by_slug($slug, $resource)
    {
        $found = false;

        foreach( $resource['actions'] as $action)
        {
            if( $action['slug'] === $slug )
            {
                $found = $action;
                break;
            }
        }

        return $found;
    }

    private function _get_ressources()
    {
        $settings_file_path = __DIR__ . '/../../settings.json';

        $resources = [];
        if(  file_exists($settings_file_path) )
        {
            $settings = json_decode(file_get_contents($settings_file_path), true);
            $resources = $settings['resources'];
        }
        else
        {
            \Log::error('[ResourceManagerService:_get_ressources] : No setting.json file found!', [
                'settings_file_path' => $settings_file_path
            ]);
        }

        return $resources;
    }

    private function _make_cmd($resource, $action)
    {
        $cmd = false;

        $type = $resource['type'];

        $resource_pin = $resource['pin'];
        $action_slug = $action['slug'];
        $resource_default = $resource['default'];
        $identity = ( array_key_exists('identity', $resource) ) ? $resource['identity'] : false;

        $python_path = env('PYTHON_PATH');
        $gpio_path = __DIR__ . '/../../gpio/';

        if( $type === 'relay')
            $cmd = "$python_path {$gpio_path}relay.py $resource_pin $action_slug $resource_default ";
        else if ( $type === 'submersible_thermometer')
            $cmd = "$python_path {$gpio_path}submersible_thermometer.py $resource_pin $action_slug $identity";

        return $cmd;
    }

    private function _run_cmd($cmd)
    {
        $cmd = escapeshellcmd($cmd);
        $output = shell_exec($cmd);

        $output = json_decode($output, true);

        return $output;
    }

    private function _format_result($output, $resource, $action)
    {
        $result = false;

        if( is_array($output))
        {
            $result = [];
            if( $output['success'] )
            {
                $result['success'] = true;
            }
            else
            {
                $result['success'] = false;
            }

            $result['msg'] = "{$resource['label']} {$action['label']} : {$output['label']}";

            $infos = [];
            $infos['action'] = $action['slug'];
            $infos['default'] = $resource['default'];
            $infos['description'] = $resource['description'];
            $infos['resource'] = $resource['label'];

            $state = [];
            $state['label'] = $output['label'];
            $state['pin'] = $resource['pin'];
            $state['value'] = $output['state'];

            $result['infos'] = $infos;
            $result['state'] = $state;
        }

        return $result;
    }
}
