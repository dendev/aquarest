<?php
namespace App\Providers;

use App\Services\ResourceManagerService;
use Illuminate\Support\ServiceProvider;

class ResourceManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'resource_manager', function ($app) {
            return new ResourceManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

}
